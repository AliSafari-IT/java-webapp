package org.crewcontrol;

import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App {
    public App() {
        System.out.println("Hello World 1!");
        System.out.println(Arrays.toString(new int[] { 1, 2, 3 }));
    }

    public App(String arg1, String arg2) {
        System.out.println("Hello World 2!");
        System.out.println(Arrays.toString(new int[] { 1, 2, 3 }));
    }

    // Add a new method showing that the application is working as expected
    public String showApp(int number) {
        if(number == 1) {
            return "GoodBye World!";
        }
        return "Hello World!";
    }

    public static void main(String[] args) {
        App app = new App();
        app.showApp(1);
        app.showApp(2);
    }
}
