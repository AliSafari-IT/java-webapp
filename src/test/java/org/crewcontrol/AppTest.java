package org.crewcontrol;

import java.util.Arrays;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        // Assert that the application is working as expected
        assertTrue( true );
        assertEquals( 1, 1 );
        assertNotSame( 1, 2 );
        equals( new int[]{1, 2, 3}, new int[]{1, 2, 3} );
        App app = new App("arg1", "arg2");
        // Assert if output is equal to "Hello World!"
        assertEquals( "Hello World!", app.showApp(1) );
        // Assert if output is equal to "GoodBye World!"
        assertEquals("GoodBye World!", app.showApp(1));
    }

    private void equals(int[] ints, int[] ints1) {
        // check if the two arrays are equal
        boolean equal = Arrays.equals(ints, ints1);
        assertTrue(equal);
    }
}
